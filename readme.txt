Multiple Address Manager
by Numinix (http://www.numinix.com)

This module creates an admin tool for defining multiple addresses a store might ship from.  Support modules can then use these addresses for shipping from multiple locations such as drop shipping or from multiple warehouses.

Installation:
0. Install Numinix Product Fields (used to assign an origin to each product)
1. Patch your database by copying and pasting the contents of install.sql into ADMIN->TOOLS->INSTALL SQL PATCHES.
2. Upload all files while maintaining the directory structure.
3. Go to ADMIN->TOOLS->MULTIPLE ADDRESS MANAGER and add your locations and a unique code for each origin.
4. Once you have added all of your locations, go to ADMIN->CATALOG->CATEGORIES/PRODUCTS and assign an origin to each product.

Uninstall:
1. Remove all files included in the package.
2. Patch your database by copying and pasting the contents of uninstall.sql into ADMIN->TOOLS->INSTALL SQL PATCHES.