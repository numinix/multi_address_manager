<?php
define('HEADING_TITLE', 'Multiple Address Manager');
define('TITLE_ADDRESS_ID', 'ID');
define('TITLE_ADDRESSES', 'Addresses');
define('TITLE_ORDER_STATUS_SHIPMENTS', 'Order Status - Shipments');
define('TITLE_ORDER_STATUS_RETURNS', 'Order Status - Returns');
define('TITLE_SHIPPING_ORIGIN', 'Shipping Origin Code');
define('TITLE_ACTION', 'Action');
define('LABEL_COMPANY_NAME', 'Company Name:');
define('LABEL_SHIPPERS_NAME', 'Shipper\'s Name:');
define('LABEL_ADDRESS_LINE_1', 'Address Line 1:');
define('LABEL_ADDRESS_LINE_2', 'Address Line 2:');
define('LABEL_CITY', 'City:');
define('LABEL_STATE', 'State:');
define('LABEL_POSTAL', 'Postal Code:');
define('LABEL_COUNTRY', 'Label Country');
define('LABEL_SHIPMENTS', 'Order Status - Shipments');
define('LABEL_RETURNS', 'Order Status - Returns');
define('LABEL_SHIPPING_ORIGIN', 'Shipping Origin Code');
//eof