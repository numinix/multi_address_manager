$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});

$(document).ready(function(){
  $("select[name=change_address_reversed]").change( function() {
    var id = $("select[name=change_address_reversed]").val();
    var orders_id = $.getUrlVar('oID');
    var action = $.getUrlVar('action');
    var status = $.getUrlVar('status');
    window.location.replace("ship_fedex_reversed.php?address_id=" + id + "&oID=" + orders_id + "&action=" + action + "&status=" + status);
  });
  $("select[name=change_address]").change( function() {
    var id = $("select[name=change_address]").val();
    var orders_id = $.getUrlVar('oID');
    var action = $.getUrlVar('action');
    var status = $.getUrlVar('status');
    window.location.replace("ship_fedex.php?address_id=" + id + "&oID=" + orders_id + "&action=" + action + "&status=" + status);
  });
});