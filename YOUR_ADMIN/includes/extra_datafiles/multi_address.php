<?php
/**
 * @package admin
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: endicia_labels.php 3 2009-10-14 18:54:23Z numinix $
 */
if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}
  define('FILENAME_MULTI_ADDRESS', 'multi_address');
  define('TABLE_MULTI_ADDRESS', DB_PREFIX . 'multi_address');

define('BOX_MULTI_ADDRESS', 'Multiple Address Manager');