          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
  // create an array of all shipping origins
  $shipping_origins = $db->Execute("SELECT shipping_origin FROM " . TABLE_MULTI_ADDRESS . " ORDER BY address_id ASC;");
  $shipping_origins_array = array();
  while (!$shipping_origins->EOF) {
    $shipping_origins_array[] = array("text" => $shipping_origins->fields['shipping_origin'],
                                      "id" => $shipping_origins->fields['shipping_origin']);
    $shipping_origins->MoveNext();
  }
?>          
          <tr bgcolor="#DDEACC">
            <td class="main"><?php echo TEXT_PRODUCTS_SHIPPING_ORIGIN; ?></td>
            <td class="main"><?php echo zen_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . zen_draw_pull_down_menu('products_shipping_origin', $shipping_origins_array, $pInfo->products_shipping_origin); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>