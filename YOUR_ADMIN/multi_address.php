<?php
ob_start();
require('includes/application_top.php');
// get all saved addresses
$addresses = $db->Execute("SELECT address_id, company, name, address_1, address_2, city, state, zip, country, shipment_orders_status_id, returns_orders_status_id, shipping_origin
                     FROM " . TABLE_MULTI_ADDRESS. "
                     ORDER BY address_id ASC;");
$orders_status = $db->Execute("SELECT orders_status_id, orders_status_name FROM " . TABLE_ORDERS_STATUS . " WHERE language_id = " . (int)$_SESSION['languages_id']);
$orders_status_array = array();
while (!$orders_status->EOF) {
  $orders_status_array[] = array("text" => $orders_status->fields['orders_status_name'],
                                 "id" => $orders_status->fields['orders_status_id']);
  $orders_status->MoveNext();
}

$action = $_GET['action'];
switch($action) {
  case "add":
    $address_id = zen_db_prepare_input($_POST['address_id']);
    $company = addslashes(zen_db_prepare_input($_POST['company']));
    $name = addslashes(zen_db_prepare_input($_POST['name']));
    $address_1 = addslashes(zen_db_prepare_input($_POST['address_1']));
    $address_2 = addslashes(zen_db_prepare_input($_POST['address_2']));
    $city = addslashes(zen_db_prepare_input($_POST['city']));
    $state = addslashes(zen_db_prepare_input($_POST['state'])); 
    // abbreviate the state
    $state_name = $db->Execute("SELECT zone_code FROM " . TABLE_ZONES . " WHERE zone_name = '" . ucwords(strtolower(trim($state))) . "' LIMIT 1;");
    $state = $state_name->fields['zone_code'];  
    $zip = addslashes(zen_db_prepare_input($_POST['zip']));
    $country = zen_db_prepare_input((int)$_POST['country']);
    $shipment_orders_status_id = zen_db_prepare_input((int)$_POST['shipments']); 
    $returns_orders_status_id = zen_db_prepare_input((int)$_POST['returns']);
    $shipping_origin = zen_db_prepare_input($_POST['shipping_origin']);
    
    if ((int)$address_id > 0) {
      // perform update
      $db->Execute("UPDATE " . TABLE_MULTI_ADDRESS . "
                    SET company = '" . $company . "',
                        name = '" . $name . "',
                        address_1 = '" . $address_1 . "',
                        address_2 = '" . $address_2 . "',
                        city = '" . $city . "',
                        state = '" . $state . "',
                        zip = '" . $zip . "',
                        country = '" . $country . "',
                        shipment_orders_status_id = '" . $shipment_orders_status_id . "',
                        returns_orders_status_id = '" . $returns_orders_status_id . "',
                        shipping_origin = '" . $shipping_origin . "'
                    WHERE address_id = " . $address_id . "
                    LIMIT 1;");
      $messageStack->add_session('Address successfully updated', 'success');
      zen_redirect(zen_href_link(FILENAME_MULTI_ADDRESS, '', 'NONSSL')); 
    } else {
      // perform insert
      $address_array = array('company'                   => $company,
                             'name'                      => $name,
                             'address_1'                 => $address_1,
                             'address_2'                 => $address_2,
                             'city'                      => $city,
                             'state'                     => $state,
                             'zip'                       => $zip,
                             'country'                   => $country,
                             'shipment_orders_status_id' => $shipment_orders_status_id,
                             'returns_orders_status_id'  => $returns_orders_status_id,
                             'shipping_origin'           => $shipping_origin);
      zen_db_perform(TABLE_MULTI_ADDRESS, $address_array);
      $messageStack->add_session('Address successfully added', 'success');
      zen_redirect(zen_href_link(FILENAME_MULTI_ADDRESS, '', 'NONSSL'));
    }
  break;
  case 'edit':
    $address_id = (int)$_GET['address_id'];
    $edit_address = $db->Execute("SELECT * FROM " . TABLE_MULTI_ADDRESS . " WHERE address_id = " . $address_id . " LIMIT 1;");
    $company = stripcslashes($edit_address->fields['company']);
    $name = stripslashes($edit_address->fields['name']); 
    $address_1 = stripslashes($edit_address->fields['address_1']); 
    $address_2 = stripslashes($edit_address->fields['address_2']);
    $city = stripslashes($edit_address->fields['city']); 
    
    $state = $db->Execute('SELECT zone_name FROM ' . TABLE_ZONES . ' WHERE zone_code = "' . $edit_address->fields['state'] . '" LIMIT 1;');
    $state = stripslashes($state->fields['zone_name']); 

    $zip = stripslashes($edit_address->fields['zip']); 
    $country = $edit_address->fields['country'];
    $shipment_orders_status_id = $edit_address->fields['shipment_orders_status_id'];
    $returns_orders_status_id = $edit_address->fields['returns_orders_status_id'];
    $shipping_origin = $edit_address->fields['shipping_origin'];
  break;
  case 'remove':
    $address_id = (int)$_GET['address_id'];
    $db->Execute("DELETE FROM " .TABLE_MULTI_ADDRESS . " WHERE address_id = " . $address_id . " LIMIT 1;");
    $messageStack->add_session('Address successfully removed', 'caution');
    zen_redirect(zen_href_link(FILENAME_MULTI_ADDRESS, '', 'NONSSL'));
  break;
  default:
    $country = STORE_COUNTRY;
  break;    
}                     
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<script language="javascript" src="includes/menu.js"></script>
    <script language="javascript" src="includes/general.js"></script>
    <script type="text/javascript">
    <!--
    function init()
    {
      cssjsmenu('navbar');
      if (document.getElementById)
      {
        var kill = document.getElementById('hoverJS');
        kill.disabled = true;
      }
    if (typeof _editor_url == "string") HTMLArea.replaceAll();
    }
    // -->
    </script>
    <?php if (HTML_EDITOR_PREFERENCE=="FCKEDITOR") require(DIR_WS_INCLUDES.'fckeditor.php'); ?>
    <?php if (HTML_EDITOR_PREFERENCE=="HTMLAREA")  require(DIR_WS_INCLUDES.'htmlarea.php'); ?>
<style type="text/css">
  label.inputLabel{clear:both;}
  div#addressFormContainer {float:left;}
  table#addressTable{margin-left: 2em; border-collapse:collapse; border:1px solid #036; font-size: small; float:left;width:700px;}
  table#addressTable th{background-color:#036; border-top:0px; border-left:1px solid #003366; border-right:1px solid #003366; border-bottom:1px double #003366; color: #fff; text-align:center; padding:8px;} 
  table#addressTable td{border:1px solid #036; vertical-align:top; padding:5px 10px;}
  fieldset#addressForm{width:250px;}
  #addressForm input[type="text"], #addressForm select {margin-bottom: 1em;width:95%;}
</style>
</head>

<body onLoad="init()">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<h1><?php echo HEADING_TITLE; ?></h1>
  <div id="addressFormContainer">
<?php
  echo zen_draw_form('multi_address', FILENAME_MULTI_ADDRESS, 'action=add', 'post');  
?>
      <fieldset id="addressForm">
      <?php if ($action == 'edit') { ?>
        <input type="hidden" name="address_id" value="<?php echo $address_id; ?>" />
      <?php } ?>
        <label class="inputLabel" for="name"><?php echo LABEL_COMPANY_NAME; ?></label>
        <?php echo zen_draw_input_field('company', $company, 'size="32" id="company"'); ?>
        <br class="clearBoth" />
        
        <label class="inputLabel" for="name"><?php echo LABEL_SHIPPERS_NAME; ?></label>
        <?php echo zen_draw_input_field('name', $name, 'size="32" id="name"'); ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="address_1"><?php echo LABEL_ADDRESS_LINE_1; ?></label>
          <?php echo zen_draw_input_field('address_1', $address_1, 'size="32" id="address_1"'); ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="address_2"><?php echo LABEL_ADDRESS_LINE_2; ?></label>
        <?php echo zen_draw_input_field('address_2', $address_2, 'size="32" id="address_2"'); ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="city"><?php echo LABEL_CITY; ?></label>
        <?php echo zen_draw_input_field('city', $city, 'size="32" id="city"'); ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="stateZone" id="zoneLabel"><?php echo LABEL_STATE; ?></label>
        <?php echo zen_draw_input_field('state', $state, 'size="32" id="state"'); ?>
        <br class="clearBoth" id="stBreak" />

        <label class="inputLabel" for="zip"><?php echo LABEL_POSTAL; ?></label>
        <?php echo zen_draw_input_field('zip', $zip, 'size="32" id="zip"'); ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="country"><?php echo LABEL_COUNTRY; ?></label>
        <?php 
        $countries = array();
        $countries[] = array(
          'text' => 'Select Country',
          'id' => '',
        );

        foreach (zen_get_countries() as $key => $con) {
          
          $countries[] = array(
            'text' => $con['countries_name'],
            'id' => $con['countries_id'],
          );
        }
        ?>
        <?php echo zen_draw_pull_down_menu('country', $countries, $country); ?>
        <br class="clearBoth" />
        
        <label class="inputLabel" for="shipments"><?php echo LABEL_SHIPMENTS; ?></label>
        <?php echo zen_draw_pull_down_menu('shipments', $orders_status_array, $shipment_orders_status_id); ?>
        <br class="clearBoth" />
        
        <label class="inputLabel" for="returns"><?php echo LABEL_RETURNS; ?></label>
        <?php echo zen_draw_pull_down_menu('returns', $orders_status_array, $returns_orders_status_id); ?>
        <br class="clearBoth" />
        
        <label class="inputLabel" for="shipping_origin"><?php echo LABEL_SHIPPING_ORIGIN; ?></label>
        <?php echo zen_draw_input_field('shipping_origin', $shipping_origin, 'size="32" id="shipping_origin"'); ?>
        <br class="clearBoth" />
        
        <div class="buttonRow forward">
        <?php
          echo zen_image_submit('button_submit.gif', 'Submit');
        ?>
        </div>
      </fieldset>
    </form>
  </div>
  <table id="addressTable">
    <tr>
      <th><?php echo TITLE_ADDRESS_ID; ?></th>
      <th><?php echo TITLE_ADDRESSES; ?></th>
      <th><?php echo TITLE_ORDER_STATUS_SHIPMENTS; ?></th>
      <th><?php echo TITLE_ORDER_STATUS_RETURNS; ?></th>
      <th><?php echo TITLE_SHIPPING_ORIGIN; ?></th>
      <th><?php echo TITLE_ACTION; ?></th>
    </tr>
<?php
  while (!$addresses->EOF) {
    $address = '';
    if ($addresses->fields['company']) $address .=  stripslashes($addresses->fields['company']) . '<br />';
    if ($addresses->fields['name']) $address .=  stripslashes($addresses->fields['name']) . '<br />';
    $address .= stripslashes($addresses->fields['address_1']) . '<br />';
    if ($addresses->fields['address_2'] != '') $address .= stripslashes($addresses->fields['address_2']) . '<br />';
    $address .= stripslashes($addresses->fields['city']) . ', ' . stripslashes($addresses->fields['state']) . ', ' . stripslashes($addresses->fields['zip']) . '<br />';
    $address .= zen_get_country_name($addresses->fields['country']);
    $shipment_name = $db->Execute("SELECT orders_status_name FROM " . TABLE_ORDERS_STATUS . " WHERE language_id = " . (int)$_SESSION['languages_id'] . " AND orders_status_id = '" . $addresses->fields['shipment_orders_status_id'] . "' LIMIT 1");
    $shipment_name = $shipment_name->fields['orders_status_name'];
    $returns_name = $db->Execute("SELECT orders_status_name FROM " . TABLE_ORDERS_STATUS . " WHERE language_id = " . (int)$_SESSION['languages_id'] . " AND orders_status_id = '" . $addresses->fields['returns_orders_status_id'] . "' LIMIT 1");
    $returns_name = $returns_name->fields['orders_status_name'];
    $shipping_origin = $addresses->fields['shipping_origin'];
?>
    <tr>
      <td><?php echo $addresses->fields['address_id']; ?></td>
      <td><address><?php echo $address;?></address></td>
      <td><?php echo $shipment_name?></td>
      <td><?php echo $returns_name;?></td>
      <td><?php echo $shipping_origin; ?></td>     
      <td><a href="<?php echo zen_href_link(FILENAME_MULTI_ADDRESS, 'action=edit&address_id=' . $addresses->fields['address_id'], 'NONSSL'); ?>">Edit</a> / <a href="<?php echo zen_href_link(FILENAME_MULTI_ADDRESS, 'action=remove&address_id=' . $addresses->fields['address_id'], 'NONSSL'); ?>">Remove</a></td>
    </tr>
<?php
    $addresses->MoveNext();
  }
?>
  </table>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
