DROP TABLE IF EXISTS multi_address;
CREATE TABLE IF NOT EXISTS multi_address (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(32) NULL default NULL,
  `name` varchar(32) NULL default NULL,
  `address_1` varchar(32) NULL default NULL,
  `address_2` varchar(32) NULL default NULL,
  `city` varchar(32) NULL default NULL,
  `state` varchar(32) NULL default NULL,
  `country` int(11) NULL default NULL,
  `zip` varchar(12) NULL default NULL,
  `shipment_orders_status_id` int(11) NULL default NULL,
  `returns_orders_status_id` int(11) NULL default NULL,
  `shipping_origin` varchar(32) NULL default NULL,
  PRIMARY KEY ( `address_id` )
);

SET @configuration_group_id=0;
SELECT (@configuration_group_id:=configuration_group_id) 
FROM configuration_group 
WHERE configuration_group_title = 'Multiple Address Manager Configuration' 
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;

INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) VALUES (NULL, 'Multiple Address Manager Configuration', 'Set Multiple Address Manager Options', '1', '1');
SET @configuration_group_id=last_insert_id();
UPDATE configuration_group SET sort_order = @configuration_group_id WHERE configuration_group_id = @configuration_group_id;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES
(NULL, 'Version', 'MULTIPLE_ADDRESS_MANAGER_VERSION', '1.1.0', 'Version Installed:', @configuration_group_id, 0, NOW(), NULL, NULL);

# Register the tools page for Admin Access Control
INSERT IGNORE INTO admin_pages (page_key, language_key, main_page, page_params, menu_key, display_on_menu, sort_order) VALUES ('toolsMULTI_ADDRESS', 'BOX_MULTI_ADDRESS', 'FILENAME_MULTI_ADDRESS', '', 'tools', 'Y', 31);  

ALTER TABLE products ADD products_shipping_origin varchar(32) NULL DEFAULT NULL;